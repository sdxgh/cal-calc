# calCalc

**calCalc** is a simple, self-localized date picker for the Calc module in LibreOffice.

While pointing on a cell or a multi-cells selection, just do `ctrl + right-click` (`cmd⌘ + right-click` on MacOS) to show the pop-up calendar. Then click on any date to insert it into the selected cells.

By now, **calCalc** only shows a gregorian calendar.

##### Tip
Clicking once the month/year label turns it red and allows to browse calendar by year instead of by month. Clicking again restores monthly browsing.


| ![calCalc_en_US.png](calCalc_en_US.png "calCalc in en_US environment under Windows 10") | 
|:--:| 
| *calCalc in en_US environment under Windows 10* |

| ![calCalc_ja.png](calCalc_ja.png "calCalc in ja environment under Windows 10") | 
|:--:| 
| *calCalc in ja environment under Windows 10* |

| ![calCalc_fr.png](calCalc_fr.png "calCalc in fr environment under Linux Mint") | 
|:--:| 
| *calCalc in fr environment under Linux Mint* |
