# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import calendar
import gettext
import os.path
import traceback
from itertools import chain
from datetime import datetime, timedelta

import unohelper
from com.sun.star.lang import Locale
from com.sun.star.task import XJob
from com.sun.star.beans import PropertyValue
from com.sun.star.awt.WindowClass import TOP
from com.sun.star.awt.PosSize import POS, POSSIZE
from com.sun.star.awt.WindowAttribute import BORDER, CLOSEABLE
from com.sun.star.awt.MessageBoxButtons import BUTTONS_OK
from com.sun.star.awt.MessageBoxType import INFOBOX
from com.sun.star.awt import (Rectangle, WindowDescriptor, XKeyListener, XMouseListener,
                              XTopWindowListener, XActionListener, XEnhancedMouseClickHandler,
                              XContainerWindowEventHandler)
from com.sun.star.sheet.CellFlags import DATETIME


IMPLEMENTATION_NAME = "jmz.calc.calendarpopup"


# -------------------
#  UI locale strings
# -------------------
UI_STRINGS = {}
UI_SETTINGS = {}


def loadstrings(createunoservice):
    # print('loadstrings')
    def pgettext(domain, context, message):
        try:
            T = gettext.translation(domain, resource_path, [language])
            txt = T.gettext('{}\x04{}'.format(context, message))
            if txt.startswith(context):   # domain exists but the string does not
                return txt[len(context)+1:]
            return txt
        except FileNotFoundError:
            return message
    try:
        ps = createunoservice("com.sun.star.util.PathSubstitution")
        language = ps.getSubstituteVariableValue("vlang").replace("-", "_")
        program_path = unohelper.fileUrlToSystemPath(ps.getSubstituteVariableValue("prog"))
        resource_path = os.path.join(program_path, "resource")
        UI_STRINGS['today'] = pgettext('sc', 'STR_COND_TODAY', 'today')
        UI_STRINGS['daywidthlabel'] = pgettext('sw', 'columnwidth|ColumnWidthDialog', 'Column Width')
        UI_STRINGS['fontheightlabel'] = pgettext('svx', 'SIP_EE_CHAR_FONTHEIGHT', 'Font size')
        UI_STRINGS['protectionerror'] = pgettext('sc', 'STR_PROTECTIONERR', 'Protected cells can not be modified.')
        UI_STRINGS['undomessage'] = pgettext('sc', 'STR_UNDO_ENTERDATA', 'Input')
        # # 'calendar' translation
        # UI_STRINGS['calendar'] = pgettext('svx', 'RID_GALLERYSTR_THEME_CALENDAR', 'Calendar')
        # days and months translation
        alang = language.split("_") + 2*[""]
        locale = Locale(*alang[:3])
        localecalendar = createunoservice("com.sun.star.i18n.LocaleCalendar")
        localecalendar.loadDefaultCalendar(locale)    # is "gregorian" always the default?
        UI_STRINGS['months'] = [""]+[m.AbbrevName for m in localecalendar.Months]
        UI_STRINGS['firstday'] = f = localecalendar.FirstDayOfWeek
        days = localecalendar.Days2[f:]+localecalendar.Days2[:f]
        UI_STRINGS['days'] = [d.NarrowName for d in days]
        print("calCalc UI strings:\n{}".format(UI_STRINGS))
    except Exception:
        traceback.print_exc()


def loadsettings(createunoservice):
    try:
        cp = createunoservice("com.sun.star.configuration.ConfigurationProvider")
        node = PropertyValue("nodepath", 0, "/calcalc.OptionPage", 0)
        reader = cp.createInstanceWithArguments("com.sun.star.configuration.ConfigurationAccess", (node,))
        try:
            UI_SETTINGS['DAY_WIDTH'] = int(reader.getPropertyValue("DayWidth"))
        except ValueError:
            UI_SETTINGS['DAY_WIDTH'] = Calendar.DAY_WIDTH
        try:
            UI_SETTINGS['FONT_HEIGHT'] = int(reader.getPropertyValue("FontHeight"))
        except ValueError:
            UI_SETTINGS['FONT_HEIGHT'] = Calendar.FONT_HEIGHT
    except Exception:
        traceback.print_exc()


# ---------------------
#  listener base class
# ---------------------
class Listener(unohelper.Base):
    def __init__(self):
        pass

    def disposing(self, source):
        pass


# ----------
#  Calendar
# ----------
class TopWindowListener(Listener, XTopWindowListener):
    def windowClosing(self, event):
        # print ("windowClosing")
        dialog = event.Source
        dialog.setVisible(False)
        # dialog.dispose()

    def windowDeactivated(self, event):
        # print("windowDeactivated")
        dialog = event.Source
        if dialog.isVisible():
            dialog.setVisible(False)
            # dialog.dispose()
        pass

    def windowActivated(self, event):
        pass

    def windowOpened(self, event):
        pass

    def windowClosed(self, event):
        pass

    def windowMinimized(self, event):
        pass

    def windowNormalized(self, event):
        pass


class ActionListener(Listener, XActionListener):
    def __init__(self, dlg):
        self.dlg = dlg

    def actionPerformed(self, event):
        cmd = event.ActionCommand
        # print(cmd)
        try:
            offset = {'previous': -1, "next": 1}
            if cmd in offset and self.dlg.offsetcolors[0] == -1:    # default color -> monthly
                m = self.dlg.month + offset[cmd]
                self.dlg.month = (m-1) % 12 + 1
                if m in (0, 13):
                    self.dlg.year += offset[cmd]
                self.dlg.loadcalendarcontent()
            elif cmd in offset:    # other color -> yearly
                self.dlg.year += offset[cmd]
                self.dlg.loadcalendarcontent()
            elif cmd == 'today':
                if self.dlg.month == self.dlg.today.month:
                    self.dlg.calendar.setVisible(False)
                    date = self.dlg.today.isoformat()[:10]
                    self.dlg.setdate(date)
                else:
                    self.dlg.month = self.dlg.today.month
                    self.dlg.year = self.dlg.today.year
                    self.dlg.loadcalendarcontent()
        except Exception:
            traceback.print_exc()


class KeyListener(Listener, XKeyListener):
    def keyPressed(self, event):
        pass

    def keyReleased(self, event):
        # print("keyReleased")
        if event.KeyCode == 1281:
            event.Source.setVisible(False)


class MouseListener(Listener, XMouseListener):
    def __init__(self, dlg):
        self.dlg = dlg

    def mousePressed(self, event):
        pass

    def mouseReleased(self, event):
        model = event.Source.Model
        if model == self.dlg.controls['month']:
            model.TextColor = self.dlg.offsetcolors[1]
            self.dlg.offsetcolors.reverse()
        else:
            self.dlg.calendar.setVisible(False)
            date = event.Source.Model.HelpURL
            self.dlg.setdate(date)


class Calendar():
    BACKGROUND = 0xffffe6
    DAY_WIDTH = 20
    FONT_HEIGHT = 8

    def __init__(self, ctx, doc, selection):
        # print(UI_SETTINGS)
        self.ctx = ctx
        self.doc = doc
        self.parentwin = doc.CurrentController.Frame.ContainerWindow
        self.smgr = ctx.ServiceManager
        self.toolkit = self.create("com.sun.star.awt.Toolkit")
        self.actionlistener = ActionListener(self)
        self.mouselistener = MouseListener(self)
        self.controls = {}
        self.calendar = None
        # NEXT LINE: python 0 day = Monday while Libo 0 day = Sunday :(
        self.pycal = calendar.Calendar((UI_STRINGS['firstday']+6) % 7)
        self.dates = []
        self.today = datetime.today().date()
        self.setcurrentmonthyear(selection)
        self.offsetcolors = [-1, 0xff0033]   # default color = monthly, red = yearly
        self.createwindow()

    def create(self, service):
        return self.smgr.createInstance(service)

    def show(self, X=10, Y=10):
        try:
            self.loadcalendarcontent()
            self.calendar.setPosSize(X, Y, 0, 0, POS)
            self.calendar.setVisible(True)
            # self.calendar.dispose()
        except Exception:
            traceback.print_exc()

    def setcurrentmonthyear(self, sel):
        dates = sel.queryContentCells(DATETIME)
        if dates.Count:
            cp = self.create("com.sun.star.configuration.ConfigurationProvider")
            node = PropertyValue("nodepath", 0, "org.openoffice.Office.Calc/Calculate/Other/Date", 0)
            basedate = cp.createInstanceWithArguments("com.sun.star.configuration.ConfigurationAccess", (node,))
            self.celldate = datetime(basedate.YY, basedate.MM, basedate.DD).date() + timedelta(sel.Value)
            self.month = self.celldate.month
            self.year = self.celldate.year
        else:
            self.celldate = None
            self.month = self.today.month
            self.year = self.today.year

    def createwindow(self):
        try:
            w = UI_SETTINGS.get('DAY_WIDTH', self.DAY_WIDTH)
            fh = UI_SETTINGS.get('FONT_HEIGHT', self.FONT_HEIGHT)
            descriptor = WindowDescriptor()
            descriptor.Type = TOP
            descriptor.WindowServiceName = ""
            descriptor.ParentIndex = 0
            descriptor.Parent = self.parentwin
            descriptor.Bounds = Rectangle(0, 0, w*7+2, w*9+2)
            descriptor.WindowAttributes = BORDER | CLOSEABLE
            self.calendar = self.toolkit.createWindow(descriptor)    # 2 xPeer|xWindow
            self.calendar.addTopWindowListener(TopWindowListener())
            self.calendar.addKeyListener(KeyListener())
            self.calendar.setBackground(self.BACKGROUND)
            # self.calendar.setControlFont(self.calendar.FontDescriptors[300])
            # self.calendar.setTitle("")

            self.create_control('btnleft', 'Button', (0, 0, w, w),
                                ('FocusOnClick', 'FontHeight', 'Label', 'Repeat', 'Tabstop'),
                                (False, fh+2, '◂', True, False), action="previous")
            self.create_control('btnright', 'Button', (w*6, 0, w, w),
                                ('FocusOnClick', 'FontHeight', 'Label', 'Repeat', 'Tabstop'),
                                (False, fh+2, '▸', True, False), action="next")
            self.create_control('hui', 'Button', (0, w*8, w*7, w),
                                ('FocusOnClick', 'FontHeight', 'Label', 'Tabstop'),
                                (False, fh, UI_STRINGS['today'], False), action="today")
            self.create_control('month', 'FixedText', (w, 0, w*5, w),
                                ('Align', 'Border', 'FocusOnClick', 'FontHeight',
                                 'FontName', 'FontWeight', 'VerticalAlign'),
                                (1, 0, False, fh, "DejaVu Sans Condensed", 150, 1), mouse=True)
            for n, j in enumerate(UI_STRINGS['days']):
                self.create_control(j, 'FixedText', (n*w, w, w, w),
                                    ('Align', 'Border', 'FocusOnClick', 'FontHeight', 'FontName', 'FontWeight',
                                     'Label', 'TextColor', 'VerticalAlign'),
                                    (1, 0, False, fh+2, "DejaVu Sans Condensed", 150, j.upper(), 0x004586, 1))
            for d in range(42):
                q, r = divmod(d, 7)
                name = '{}'.format(d)
                self.create_control(name, 'FixedText', (r*w, q*w+2*w, w, w),
                                    ('Align', 'Border', 'FocusOnClick', 'FontHeight', 'FontName', 'VerticalAlign'),
                                    (1, 0, False, fh, "DejaVu Sans Condensed", 1), mouse=True)
        except Exception:
            traceback.print_exc()

    def create_control(self, name, type_, possize=None, prop_names=None, prop_values=None, action='', mouse=False):
        try:
            type_ = "com.sun.star.awt.UnoControl{}Model".format(type_)
            model = self.create(type_)
            if prop_names and prop_values:
                model.setPropertyValues(prop_names, prop_values)
            ctrl = self.create(model.DefaultControl)
            ctrl.setModel(model)
            ctrl.setContext(self.calendar)
            ctrl.createPeer(self.toolkit, self.calendar)
            if possize:
                ctrl.setPosSize(possize[0], possize[1], possize[2], possize[3], POSSIZE)
            if action:
                ctrl.setActionCommand(action)
                ctrl.addActionListener(self.actionlistener)
            if mouse:
                ctrl.addMouseListener(self.mouselistener)
            self.controls[name] = model
        except Exception:
            traceback.print_exc()

    def loadcalendarcontent(self):
        # print("loadcalendarcontent")
        months = UI_STRINGS['months']
        self.controls['month'].Label = '{} {}'.format(months[self.month], self.year)
        dates = self.pycal.monthdatescalendar(self.year, self.month)
        self.dates = tuple(chain.from_iterable(dates))
        for n, date in enumerate(self.dates):
            model = self.controls['{}'.format(n)]
            color = 0x0
            bold = 100
            if date.month != self.month:
                color = 0x888888
            else:
                if date == self.today:
                    color = 0x800000
                    bold = 150
                elif date == self.celldate:
                    color = 0x014586
                    bold = 150
            model.setPropertyValues(("FontWeight", "HelpURL", "Label", "TextColor"),
                                    (bold, date.isoformat(), '{}'.format(date.day), color))
        # erase superfluous lines
        if n < 41:
            for n in range(n+1, 42):
                model = self.controls['{}'.format(n)]
                model.Label = ''

    def setdate(self, date):
        self.doc.lockControllers()
        undomanager = self.doc.UndoManager
        undomanager.enterUndoContext("{} (calCalc)".format(UI_STRINGS['undomessage']))
        try:
            cursel = self.doc.CurrentSelection
            if cursel.supportsService("com.sun.star.sheet.SheetCell"):
                cursel.setFormula(date)
                if cursel.NumberFormat == 0:
                    cursel.NumberFormat = self.doc.NumberFormats.getStandardFormat(2, cursel.CharLocale)
            elif cursel.supportsService("com.sun.star.sheet.SheetCellRange"):
                self.setdate_multi(cursel, date)
            elif cursel.supportsService("com.sun.star.sheet.SheetCellRanges"):
                for crange in cursel:
                    self.setdate_multi(crange, date)
        finally:
            undomanager.leaveUndoContext()
            self.doc.unlockControllers()

    def setdate_multi(self, frange, date):
        # dates = ((date,)*frange.Columns.Count,)*frange.Rows.Count
        # frange.setFormulaArray(dates)

        # CellRange operations like setFormulaArray are not undoable: https://bugs.documentfoundation.org/show_bug.cgi?id=114038
        # Behaviour is very bad, so let's fill one cell at a time :(
        for col in range(frange.Columns.Count):
            for row in range(frange.Rows.Count):
                frange.getCellByPosition(col, row).setFormula(date)
        
        for r in frange.UniqueCellFormatRanges:
            if r.NumberFormat == 0:
                r.NumberFormat = self.doc.NumberFormats.getStandardFormat(2, r.CharLocale)


# ------
#  XJob
# ------
class MouseClickHandler(Listener, XEnhancedMouseClickHandler):
    def __init__(self, ctx, doc):
        # print("MouseClickHandler.__init__()")
        self.ctx = ctx
        self.doc = doc

    def mousePressed(self, e):
        return True

    def mouseReleased(self, e):
        # print("MouseClickHandler.mouseReleased()")
        if not e.Target.supportsService("com.sun.star.sheet.SheetCell"):
            return True
        if (e.Buttons, e.ClickCount, e.Modifiers) == (2, 1, 2):
            try:
                frame = self.doc.CurrentController.Frame
                ismultiselection = self.doc.CurrentSelection.queryIntersection(e.Target.RangeAddress).Count
                if not ismultiselection:
                    dispatcher = self.ctx.ServiceManager.createInstance("com.sun.star.frame.DispatchHelper")
                    dispatcher.executeDispatch(frame, ".uno:GoToCell", "", 0,
                                               (PropertyValue("ToPoint", 0, e.Target.AbsoluteName, 0),))
                # selection has changed, do not use a previous pointer to current selection
                if self.isselectionprotected(self.doc.CurrentSelection):
                    self.errormessage(UI_STRINGS['protectionerror'])
                else:
                    calendar = Calendar(self.ctx, self.doc, e.Target)
                    Y = e.Y + frame.LayoutManager.CurrentDockingArea.Height + frame.LayoutManager.CurrentDockingArea.Y + 16
                    X = e.X + 31
                    calendar.show(X, Y)
                return True
            except Exception:
                traceback.print_exc()
        return True

    def isselectionprotected(self, cursel):
        if cursel.supportsService("com.sun.star.sheet.SheetCell"):
            return (cursel.Spreadsheet.isProtected() and cursel.CellProtection.IsLocked)
        elif cursel.supportsService("com.sun.star.sheet.SheetCellRange"):
            return (cursel.Spreadsheet.isProtected() and cursel.CellProtection.IsLocked)
        elif cursel.supportsService("com.sun.star.sheet.SheetCellRanges"):
            for crange in cursel:
                if (crange.Spreadsheet.isProtected() and crange.CellProtection.IsLocked):
                    return True
            return False

    def errormessage(self, message):
        try:
            componentwindow = self.doc.CurrentController.ComponentWindow
            toolkit = componentwindow.Toolkit
            messagebox = toolkit.createMessageBox(componentwindow, INFOBOX, BUTTONS_OK, "calCalc", message);
            messagebox.execute()
        except Exception:
            traceback.print_exc()


class TheCalendar(unohelper.Base, XJob):
    def __init__(self, ctx):
        print("TheCalendar.__init__()")
        self.ctx = ctx
        self.createunoservice = ctx.ServiceManager.createInstance
        desktop = self.createunoservice("com.sun.star.frame.Desktop")
        self.doc = desktop.CurrentComponent

    def execute(self, args):
        # print(str(args))
        try:
            if self.doc:
                if not UI_STRINGS:
                    loadstrings(self.createunoservice)
                if not UI_SETTINGS:
                    loadsettings(self.createunoservice)
                self.doc.CurrentController.addEnhancedMouseClickHandler(MouseClickHandler(self.ctx, self.doc))
        except Exception:
            traceback.print_exc()


# ----------------------
#  calCalc registration
# ----------------------
g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(TheCalendar, IMPLEMENTATION_NAME, (IMPLEMENTATION_NAME,))


###############
# -------------
#  Option page
# -------------

ImplementationName = "calcalc.OptionPage"


class OptionsDialogHandler(unohelper.Base, XContainerWindowEventHandler):
    def __init__(self, ctx):
        self.ctx = ctx
        self.createunoservice = ctx.ServiceManager.createInstance
        self.CfgNode = "/calcalc.OptionPage"
        self.CfgNames = ("DayWidth", "FontHeight")
        if not UI_STRINGS:
            loadstrings(self.createunoservice)
        # print("OptionsDialogHandler loaded")

    # XContainerWindowEventHandler methods
    def callHandlerMethod(self, window, eventObject, method):
        if method == "external_event":
            try:
                self._handleExternalEvent(window, eventObject)
            except Exception as e:
                print(e)
            return True

    def getSupportedMethodNames(self):
        return ("external_event",)

    # private methods
    def _handleExternalEvent(self, window, evName):
        if evName == "ok":
            self._saveData(window)
        elif evName == "back":
            self._loadData(window, "back")
        elif evName == "initialize":
            self._loadData(window, "initialize")
        return True

    def _saveData(self, window):
        name = window.Model.Name
        if name != "calcalc_config":
            return
        columnwidth = window.getControl("columnwidth").Text
        fontheight = window.getControl("fontheight").Text
        settings = (columnwidth, fontheight)
        self._configwriter(settings)
        try:
            UI_SETTINGS['DAY_WIDTH'] = int(columnwidth)
        except ValueError:
            UI_SETTINGS['DAY_WIDTH'] = Calendar.DAY_WIDTH
        try:
            UI_SETTINGS['FONT_HEIGHT'] = int(fontheight)
        except ValueError:
            UI_SETTINGS['FONT_HEIGHT'] = Calendar.FONT_HEIGHT

    def _loadData(self, window, evName):
        name = window.Model.Name
        if name != "calcalc_config":
            return
        if evName == "initialize":
            window.getControl("columnwidthlabel").Model.Label = UI_STRINGS['daywidthlabel']
            window.getControl("fontheightlabel").Model.Label = UI_STRINGS['fontheightlabel']
        settings = self._configreader()
        if settings:
            columnwidth = window.getControl("columnwidth")
            columnwidth.setText(settings["DayWidth"] or Calendar.DAY_WIDTH)
            fontheight = window.getControl("fontheight")
            fontheight.setText(settings["FontHeight"] or Calendar.FONT_HEIGHT)
        return

    def _getconfigurationaccess(self, nodevalue, updatable=False):
        cp = self.createunoservice("com.sun.star.configuration.ConfigurationProvider")
        node = PropertyValue("nodepath", 0, nodevalue, 0)
        if updatable:
            return cp.createInstanceWithArguments("com.sun.star.configuration.ConfigurationUpdateAccess", (node,))
        else:
            return cp.createInstanceWithArguments("com.sun.star.configuration.ConfigurationAccess", (node,))

    def _configreader(self):
        settings = {}
        try:
            reader = self._getconfigurationaccess(self.CfgNode)
            values = reader.getPropertyValues(self.CfgNames)
            for name, value in zip(self.CfgNames, values):
                settings[name] = value
        except Exception as e:
            print("error on _configreader")
            raise e
        return settings

    def _configwriter(self, values):
        try:
            writer = self._getconfigurationaccess(self.CfgNode, True)
            values = writer.setPropertyValues(self.CfgNames, values)
            writer.commitChanges()
        except Exception as e:
            print("error on _configwriter")
            raise e
        pass


# -----------------------------
#  option service registration
# -----------------------------
g_ImplementationHelper.addImplementation(
    OptionsDialogHandler, ImplementationName, (ImplementationName,),)
